package cn.tedu.account.tcc;

import cn.tedu.account.entity.Account;
import cn.tedu.account.mapper.AccountMapper;
import io.seata.rm.tcc.api.BusinessActionContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Component
@Slf4j
public class AccountTccActionImpl implements AccountTccAction {
    @Autowired
    private AccountMapper accountMapper;

    @Transactional
    @Override
    public boolean prepareDecreaseAccount(BusinessActionContext businessActionContext, Long userId, BigDecimal money) {
        log.info("扣减金额第一阶段，开始扣执冻结金额");

        Account account = accountMapper.selectById(userId);
        if (account.getResidue().compareTo(money) < 0) {
            throw new RuntimeException("没有可用金额，金额冻结失败");
        }

        accountMapper.updateFrozen(
                userId,
                account.getResidue().subtract(money),
                account.getFrozen().add(money));

        // if (Math.random() < 0.5) {
        //     throw new RuntimeException("模拟异常");
        // }

        ResultHolder.setResult(AccountTccAction.class, businessActionContext.getXid(), "p");
        log.info("扣减金额第一阶段，冻结金额成功");
        return true;
    }

    @Transactional
    @Override
    public boolean commit(BusinessActionContext businessActionContext) {
        log.info("扣减金额第二阶段提交，开始执行提交");

        if (ResultHolder.getResult(AccountTccAction.class, businessActionContext.getXid()) == null) {
            return true;
        }

        long userId = Long.parseLong(businessActionContext.getActionContext("userId").toString());
        BigDecimal money = new BigDecimal(businessActionContext.getActionContext("money").toString());

        accountMapper.updateFrozenToUsed(userId, money);
        ResultHolder.removeResult(AccountTccAction.class, businessActionContext.getXid());
        log.info("扣减金额第二阶段提交，提交成功");
        return true;
    }

    @Transactional
    @Override
    public boolean rollback(BusinessActionContext businessActionContext) {
        log.info("扣减金额第二阶段回滚，开始执行回滚");

        if (ResultHolder.getResult(AccountTccAction.class, businessActionContext.getXid()) == null) {
            return true;
        }

        long userId = Long.parseLong(businessActionContext.getActionContext("userId").toString());
        BigDecimal money = new BigDecimal(businessActionContext.getActionContext("money").toString());

        accountMapper.updateFrozenToResidue(userId, money);

        ResultHolder.removeResult(AccountTccAction.class, businessActionContext.getXid());
        log.info("扣减金额第二阶段回滚，回滚成功");
        return true;
    }
}
