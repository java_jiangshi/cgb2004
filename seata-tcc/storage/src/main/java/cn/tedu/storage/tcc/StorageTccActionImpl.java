package cn.tedu.storage.tcc;

import cn.tedu.storage.entity.Storage;
import cn.tedu.storage.mapper.StorageMapper;
import io.seata.rm.tcc.api.BusinessActionContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.net.ResponseCache;

@Component
@Slf4j
public class StorageTccActionImpl implements StorageTccAction {
    @Autowired
    private StorageMapper storageMapper;

    @Transactional
    @Override
    public boolean prepareDecreaseStorage(BusinessActionContext businessActionContext, Long productId, Integer count) {
        log.info("减少库存第一阶段，开始执行冻结操作");

        Storage s = storageMapper.selectById(productId);
        if (s.getResidue() < count) {
            throw new RuntimeException("库存量不足，无法完成库存冻结");
        }

        storageMapper.updateFrozen(productId, s.getResidue()-count, s.getFrozen()+count);
        ResultHolder.setResult(StorageTccAction.class, businessActionContext.getXid(), "p");
        log.info("减少库存第一阶段，库存冻结成功");
        return true;
    }

    @Transactional
    @Override
    public boolean commit(BusinessActionContext businessActionContext) {
        log.info("减少库存第二阶段，开始提交操作");

        if (ResultHolder.getResult(StorageTccAction.class, businessActionContext.getXid()) == null) {
            return true;
        }

        long productId = Long.parseLong(businessActionContext.getActionContext("productId").toString());
        int count = Integer.parseInt(businessActionContext.getActionContext("count").toString());

        storageMapper.updateFrozenToUsed(productId, count);
        ResultHolder.removeResult(StorageTccAction.class, businessActionContext.getXid());
        log.info("减少库存第二阶段，提交成功");
        return true;
    }

    @Transactional
    @Override
    public boolean rollback(BusinessActionContext businessActionContext) {
        log.info("减少库存第二阶段，开始回滚操作");

        if (ResultHolder.getResult(StorageTccAction.class, businessActionContext.getXid()) == null) {
            return true;
        }

        long productId = Long.parseLong(businessActionContext.getActionContext("productId").toString());
        int count = Integer.parseInt(businessActionContext.getActionContext("count").toString());

        storageMapper.updateFrozenToResidue(productId, count);
        ResultHolder.removeResult(StorageTccAction.class, businessActionContext.getXid());
        log.info("减少库存第二阶段，回滚成功");
        return true;
    }
}
