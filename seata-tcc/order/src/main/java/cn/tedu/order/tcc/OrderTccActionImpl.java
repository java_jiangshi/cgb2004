package cn.tedu.order.tcc;

import cn.tedu.order.entity.Order;
import cn.tedu.order.mapper.OrderMapper;
import io.seata.rm.tcc.api.BusinessActionContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Component
@Slf4j
public class OrderTccActionImpl implements OrderTccAction {
    @Autowired
    private OrderMapper orderMapper;

    @Transactional
    @Override
    public boolean prepareCreateOrder(
            BusinessActionContext businessActionContext,
            Long orderId,Long userId,Long productId,Integer count,BigDecimal money) {
        // 插入订单，状态设置成0-冻结状态
        orderMapper.create(new Order(orderId,userId,productId,count,money,0));
        log.info("创建订单第一阶段，冻结订单成功");
        //第一阶段成功，设置成功标记
        ResultHolder.setResult(OrderTccAction.class, businessActionContext.getXid(), "p");
        return true;
    }

    @Transactional
    @Override
    public boolean commit(BusinessActionContext businessActionContext) {
        log.info("创建订单第二阶段，开始执行提交操作");

        //判断第一阶段的成功标记是否存在，没有标记就不执行提交操作
        if (ResultHolder.getResult(OrderTccAction.class, businessActionContext.getXid()) == null) {
            return true;
        }

        // 修改订单的状态，从0改成1-正常状态
        // 需要订单id来修改订单
        Long orderId = Long.parseLong(businessActionContext.getActionContext("orderId").toString());
        orderMapper.updateStatus(orderId, 1);
        log.info("创建订单第二阶段，提交订单（解冻）成功");

        //删除标记
        ResultHolder.removeResult(OrderTccAction.class, businessActionContext.getXid());

        return true;
    }

    @Transactional
    @Override
    public boolean rollback(BusinessActionContext businessActionContext) {
        log.info("创建订单第二阶段，开始执行回滚操作");

        //判断第一阶段的成功标记是否存在，没有标记就不执行提交操作
        if (ResultHolder.getResult(OrderTccAction.class, businessActionContext.getXid()) == null) {
            return true;
        }

        // 回滚订单，根据订单id删除订单
        Long orderId = Long.parseLong(businessActionContext.getActionContext("orderId").toString());
        orderMapper.deleteById(orderId);
        log.info("创建订单第二阶段，回滚订单（删除）成功");

        //删除标记
        ResultHolder.removeResult(OrderTccAction.class, businessActionContext.getXid());
        return true;
    }
}
