package m2_work;

import com.rabbitmq.client.*;

import java.io.IOException;

public class Consumer {
    public static void main(String[] args) throws Exception {
        //1. 连接
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("192.168.64.140"); // www.wht6.cn
        f.setPort(5672);
        f.setUsername("admin");
        f.setPassword("admin");
        // f.setVirtualHost("/wht");

        Channel c = f.newConnection().createChannel();

        // 定义队列
        c.queueDeclare("task_queue",
                true,
                false,
                false,
                null);

        DeliverCallback deliverCallback = new DeliverCallback() {
            @Override
            public void handle(String consumerTag, Delivery message) throws IOException {
                String msg = new String(message.getBody());
                System.out.println("收到： "+msg);
                for (int i = 0; i < msg.length(); i++) {
                    if ('.' == msg.charAt(i)) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                        }
                    }
                }

                c.basicAck(message.getEnvelope().getDeliveryTag(), false);
                System.out.println("消息处理完毕------------------------");
            }
        };

        CancelCallback cancelCallback = new CancelCallback() {
            @Override
            public void handle(String consumerTag) throws IOException {
            }
        };


        // 每次只抓取一条数据
        c.basicQos(1);

        // 消费
        c.basicConsume("task_queue",
                false, //手动ACK
                deliverCallback,
                cancelCallback);
    }
}
