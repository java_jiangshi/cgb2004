package m3_publishsubstribe;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.UUID;

public class Consumer {
    public static void main(String[] args) throws Exception {
        // 连接
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("192.168.64.140"); // www.wht6.cn
        // f.setPort(5672); //默认端口可以省略
        f.setUsername("admin");
        f.setPassword("admin");
        // f.setVirtualHost("/wht");

        Channel c = f.newConnection().createChannel();

        // 1.定义交换机  2.定义随机队列  3.绑定
        c.exchangeDeclare("logs", BuiltinExchangeType.FANOUT.getType());
        //自己给出所有的参数
        // String queue = UUID.randomUUID().toString();
        // c.queueDeclare(queue,
        //         false,
        //         true,
        //         true,
        //         null);

        //由服务器自动命名，再获取这个名字
        String queue = c.queueDeclare().getQueue();
        // 第三个参数对于发布订阅模式无效
        c.queueBind(queue, "logs", "");

        // 正常的消费数据
        DeliverCallback deliverCallback = new DeliverCallback() {
            @Override
            public void handle(String consumerTag, Delivery message) throws IOException {
                byte[] a = message.getBody();
                String msg = new String(a);
                System.out.println("收到： "+msg);
            }
        };
        CancelCallback cancelCallback = new CancelCallback() {
            @Override
            public void handle(String consumerTag) throws IOException {
            }
        };

        //3. 消费数据
        c.basicConsume(queue,
                true,
                deliverCallback,
                cancelCallback);

    }
}
