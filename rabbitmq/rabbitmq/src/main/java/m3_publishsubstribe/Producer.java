package m3_publishsubstribe;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;

import java.util.Scanner;

public class Producer {
    public static void main(String[] args) throws Exception {
        // 连接
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("192.168.64.140"); // www.wht6.cn
        // f.setPort(5672); //默认端口可以省略
        f.setUsername("admin");
        f.setPassword("admin");
        // f.setVirtualHost("/wht");

        Channel c = f.newConnection().createChannel();

        // 定义交换机
        c.exchangeDeclare("logs", BuiltinExchangeType.FANOUT);

        // 发送消息
        while (true) {
            System.out.print("输入消息：");
            String msg = new Scanner(System.in).nextLine();
            // 第二个参数，对fanout交换机无效
            c.basicPublish("logs", "", null, msg.getBytes());
        }
    }
}
