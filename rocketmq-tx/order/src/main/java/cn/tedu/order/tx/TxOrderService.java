package cn.tedu.order.tx;

import cn.tedu.order.entity.Order;
import cn.tedu.order.feign.EasyIdGeneratorClient;
import cn.tedu.order.mapper.OrderMapper;
import cn.tedu.order.service.OrderService;
import cn.tedu.order.util.JsonUtil;
import com.baomidou.mybatisplus.extension.api.R;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@Slf4j
public class TxOrderService implements OrderService {
    @Autowired
    private RocketMQTemplate rocketMQTemplate;
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    EasyIdGeneratorClient easyIdGeneratorClient;

    @Override
    public void create(Order order) {
        /*
        发送事务消息, 消息发送后, 会触发事务监听器执行,
        在事务监听器中执行本地事务（保存订单业务）
         */
        String xid = UUID.randomUUID().toString().replace("-", "");
        TxAccountMessage msg = new TxAccountMessage(order.getUserId(), order.getMoney(), xid);

        String json = JsonUtil.to(msg);
        log.info("发送消息： "+json);
        Message<String> message = MessageBuilder.withPayload(json).build();
        /*
        第三个参数，会被传递到监听器中进行处理
        监听器要执行本地事务（保存订单的业务操作），需要订单对象
         */
        rocketMQTemplate.sendMessageInTransaction("order-topic",message,order);
        log.info("事务消息已发送");
    }

    /*
    在这里真正执行保存订单的操作
    在监听器中调用 doCreate()
     */
    @Transactional
    public void doCreate(Order order) {
        log.info("开始保存订单");

        //         // 从全局唯一id发号器获得id
        Long orderId = easyIdGeneratorClient.nextId("order_business");
        order.setId(orderId);

        orderMapper.create(order);

        log.info("订单保存完成");
    }
}
