package cn.tedu.order.tx;

import cn.tedu.order.entity.Order;
import cn.tedu.order.mapper.TxMapper;
import cn.tedu.order.util.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Component
@RocketMQTransactionListener
public class TxListener implements RocketMQLocalTransactionListener {
    @Autowired
    private TxOrderService txOrderService;
    @Autowired
    private TxMapper txMapper;

    @Override
    public RocketMQLocalTransactionState executeLocalTransaction(Message message, Object o) {
        log.info("事务监听 - 开始执行本地事务");


        RocketMQLocalTransactionState status;
        int s;
        try {
            txOrderService.doCreate((Order) o);
            status = RocketMQLocalTransactionState.COMMIT;
            s = 0;
        } catch (Exception e) {
            status = RocketMQLocalTransactionState.ROLLBACK;
            s = 1;
            log.error("保存订单失败", e);
        }

        byte[] a = (byte[]) message.getPayload();
        String json = new String(a);
        String xid = JsonUtil.getString(json, "xid");

        txMapper.insert(new TxInfo(xid, s, System.currentTimeMillis()));
        return status;
    }

    @Override
    public RocketMQLocalTransactionState checkLocalTransaction(Message message) {
        byte[] a = (byte[]) message.getPayload();
        String json = new String(a);
        String xid = JsonUtil.getString(json, "xid");

        TxInfo txInfo = txMapper.selectById(xid);
        if (txInfo == null) {
            return RocketMQLocalTransactionState.UNKNOWN;
        }
        switch (txInfo.getStatus()) {
            case 0: return RocketMQLocalTransactionState.COMMIT;
            case 1: return RocketMQLocalTransactionState.ROLLBACK;
            default: return RocketMQLocalTransactionState.UNKNOWN;
        }
    }
}
