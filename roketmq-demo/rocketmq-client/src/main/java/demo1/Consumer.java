package demo1;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.List;

public class Consumer {
    public static void main(String[] args) throws Exception {
        DefaultMQPushConsumer c = new DefaultMQPushConsumer("consumer-demo1");
        c.setNamesrvAddr("192.168.64.141:9876");

        c.subscribe("Topic1", "TagA || TagB || TagC");
        c.registerMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> list, ConsumeConcurrentlyContext consumeConcurrentlyContext) {
                for (MessageExt msg:list) {
                    String s = new String(msg.getBody());
                    System.out.println("收到： "+s+" - "+msg);
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;//消费成功
                //return ConsumeConcurrentlyStatus.RECONSUME_LATER;//稍后再次消费
            }
        });

        c.start();
        System.out.println("开始消费数据");
    }
}
