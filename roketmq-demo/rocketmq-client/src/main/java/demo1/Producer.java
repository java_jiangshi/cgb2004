package demo1;

import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;

import java.util.Scanner;

public class Producer {
    public static void main(String[] args) throws Exception {
        DefaultMQProducer p = new DefaultMQProducer("producer-demo1");
        p.setNamesrvAddr("192.168.64.141:9876");
        p.start();

        // 一级分类
        String topic = "Topic4";
        // 二级分类
        String tag = "TagA";

        while (true) {
            System.out.println("输入消息，用逗号分隔多条消息：");
            String s = new Scanner(System.in).nextLine(); // aaa,bbb,ccc
            String[] a = s.split(",");

            for (String msg:a) {
                Message message = new Message(topic, tag, msg.getBytes());
                SendResult r = p.send(message);
                System.out.println(r);
            }
        }
    }
}
