package demo3;

import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.MessageQueueSelector;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageQueue;

import java.util.List;
import java.util.Scanner;

public class Producer {
    static String[] msgs = {
            "15103111039,创建",
            "15103111065,创建",
            "15103111039,付款",
            "15103117235,创建",
            "15103111065,付款",
            "15103117235,付款",
            "15103111065,完成",
            "15103111039,推送",
            "15103117235,完成",
            "15103111039,完成"
    };

    public static void main(String[] args) throws Exception {
        DefaultMQProducer p = new DefaultMQProducer("producer-demo3");
        p.setNamesrvAddr("192.168.64.141:9876");
        p.start();

        for (String s:msgs) {
            System.out.println("按回车发送消息: "+s);
            new Scanner(System.in).nextLine();

            Message msg = new Message("Topic3", "TagA", s.getBytes());
            String[] a = s.split(",");
            long id = Long.parseLong(a[0]);

            SendResult r = p.send(msg, new MessageQueueSelector() {
                @Override
                public MessageQueue select(List<MessageQueue> list, Message message, Object o) {
                    System.out.println(list);
                    Long id = (Long) o;
                    int index = (int) (id % list.size());
                    return list.get(index);
                }
            }, id);

            System.out.println(r);
            System.out.println("--------------------------------------");
        }
    }
}








