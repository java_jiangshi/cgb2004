package demo5;

import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.message.Message;

import java.util.Random;
import java.util.Scanner;

public class Producer {
    public static void main(String[] args) throws Exception {
        DefaultMQProducer p = new DefaultMQProducer("producer-demo5");
        p.setNamesrvAddr("192.168.64.141:9876");
        p.start();

        while (true) {
            System.out.print("输入消息：");
            String s = new Scanner(System.in).nextLine();
            Message msg = new Message("Topic5", s.getBytes());
            msg.putUserProperty("rnd", ""+new Random().nextInt(4));
            p.send(msg);
        }
    }
}
