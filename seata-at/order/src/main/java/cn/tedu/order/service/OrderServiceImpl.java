package cn.tedu.order.service;

import cn.tedu.order.entity.Order;
import cn.tedu.order.feign.AccountClient;
import cn.tedu.order.feign.EasyIdGeneratorClient;
import cn.tedu.order.feign.StorageClient;
import cn.tedu.order.mapper.OrderMapper;
import com.netflix.discovery.converters.Auto;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private AccountClient accountClient;
    @Autowired
    private StorageClient storageClient;
    @Autowired
    private EasyIdGeneratorClient easyIdGeneratorClient;

    // 1.创建TM 2.连接TC 3.向TC注册全局事务
    @GlobalTransactional
    @Override
    public void create(Order order) {
        // 从全局唯一id发号器获得id，这里暂时随机产生一个 orderId
        Long orderId = easyIdGeneratorClient.nextId("order_business");

        //Long orderId = Long.valueOf(new Random().nextInt(Integer.MAX_VALUE));

        order.setId(orderId);

        orderMapper.create(order);

        //调用storage，修改库存
        storageClient.decrease(order.getProductId(), order.getCount());

        //调用account，修改账户余额
        accountClient.decrease(order.getUserId(), order.getMoney());

    }
}
